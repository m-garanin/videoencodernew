//
//  Socket.h
//  VideoEncoder
//
//  Created by Josip Cavar on 16/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>

@class Socket;

@protocol SocketDelegate <NSObject>

- (void)socket:(Socket *)socket didConnectWithHandle:(CFSocketNativeHandle *)handle;

@end

@interface Socket : NSObject

@property (weak, nonatomic) id<SocketDelegate> delegate;
@property (assign, nonatomic) CFSocketRef socketRef;

- (void)startListening:(int)port runLoop:(CFRunLoopRef)runLoop;
- (void)stopListening;

@end
