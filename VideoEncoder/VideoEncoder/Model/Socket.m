//
//  Socket.m
//  VideoEncoder
//
//  Created by Josip Cavar on 16/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

#import "Socket.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void callbackSocketFuction(CFSocketRef s, CFSocketCallBackType callbackType, CFDataRef address, const void *data, void *info ) {
    
    if (callbackType == kCFSocketAcceptCallBack) {
        Socket *socket = (__bridge Socket *)(info);
        [socket.delegate socket:socket didConnectWithHandle:(CFSocketNativeHandle *)data];
    }
}

@interface Socket ()

@property (assign, nonatomic) CFRunLoopSourceRef socketSource;

@end

@implementation Socket

- (void)createSocket:(int)port {

    CFSocketContext socketContext = {0, (__bridge void *)(self), NULL, NULL, NULL};
    self.socketRef = CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack, &callbackSocketFuction, &socketContext);
    int reuseOn = 1;
    setsockopt(CFSocketGetNative(self.socketRef), SOL_SOCKET, SO_REUSEADDR, &reuseOn, sizeof(reuseOn));

    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_len = sizeof(sin);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    sin.sin_addr.s_addr= htonl(INADDR_ANY);
    CFDataRef sincfd = CFDataCreate(kCFAllocatorDefault, (UInt8 *)&sin, sizeof(sin));
    CFSocketError error = CFSocketSetAddress(self.socketRef, sincfd);
    if (error != kCFSocketSuccess) {
        NSLog(@"error white setting address, errno: %d", errno);
    }
    CFRelease(sincfd);
}

- (void)startListening:(int)port runLoop:(CFRunLoopRef)runLoop {
    
    [self createSocket:port];
    self.socketSource = CFSocketCreateRunLoopSource(kCFAllocatorDefault, self.socketRef, 0);
    CFRunLoopAddSource(runLoop, self.socketSource, kCFRunLoopDefaultMode);
}

- (void)stopListening {
    
    CFSocketInvalidate(self.socketRef);
    //CFRunLoopRemoveSource(CFRunLoopGetCurrent(), self.socketSource, 0);
    CFRelease(self.socketRef);
}

@end
