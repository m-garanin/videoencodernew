//
//  Callback.h
//  VideoEncoder
//
//  Created by Josip Cavar on 02/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VideoToolbox/VideoToolbox.h>

@class VideoCompressionSession;

@protocol VideoCompressionSessionDelegate <NSObject>

- (void)compressionSession:(VideoCompressionSession *)session didEncodeSampleBuffer:(CMSampleBufferRef)sampleBuffer;

@end

@interface VideoCompressionSession : NSObject

@property (weak, nonatomic) id<VideoCompressionSessionDelegate> delegate;
@property (assign, nonatomic) VTCompressionSessionRef compressionSessionRef;

- (OSStatus)createCompressionSessionForWidth:(int)width height:(int)height;

@end
