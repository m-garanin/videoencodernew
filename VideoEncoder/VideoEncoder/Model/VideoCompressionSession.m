//
//  Callback.m
//  VideoEncoder
//
//  Created by Josip Cavar on 02/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

#import "VideoCompressionSession.h"

void callbackCompressionFuction(void *outputCallbackRefCon, void *sourceFrameRefCon, OSStatus status, VTEncodeInfoFlags infoFlags, CMSampleBufferRef sampleBuffer) {
    
    if (status == noErr && sampleBuffer != NULL) {
        VideoCompressionSession *compressionSession = (__bridge VideoCompressionSession *)(outputCallbackRefCon);
        [compressionSession.delegate compressionSession:compressionSession didEncodeSampleBuffer:sampleBuffer];
    }
}

@implementation VideoCompressionSession

- (OSStatus)createCompressionSessionForWidth:(int)width height:(int)height {

    return VTCompressionSessionCreate(NULL, width, height, kCMVideoCodecType_H264, NULL, NULL, NULL, &callbackCompressionFuction, (__bridge void *)(self), &_compressionSessionRef);
}

@end
