//
//  CameraViewController.swift
//  VideoEncoder
//
//  Created by Josip Cavar on 01/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

import UIKit
import CoreMedia
import AVFoundation
import VideoToolbox

class CameraViewController: UIViewController, CameraSessionControllerDelegate, VideoCompressionSessionDelegate, AudioCompressionSessionDelegate, TCPControllerDelegate {

    @IBOutlet weak var cameraView: CameraView!
    @IBOutlet weak var sliderZoomControl: UISlider!
    @IBOutlet weak var labelIPAddress: UILabel!
    @IBOutlet weak var labelVideoClientsConnected: UILabel!
    @IBOutlet weak var labelAudioClientsConnected: UILabel!

    var videoPort: Int32?
    var audioPort: Int32?

    var tcpVideoController = TCPController()
    var tcpAudioController = TCPController()
    
    var cameraSessionController: CameraSessionController?
    
    var videoCompressionSession: VideoCompressionSession?
    var audioCompressionSession: AudioCompressionSession?

    
    // MARK - view lifecycle methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.labelIPAddress.text = "device ip: " + (Helper.getIFAddresses().last ?? "")
        
        AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: {
            (granted: Bool) -> Void in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                if !granted {
                    var alertController: UIAlertController = UIAlertController(title: "Could not use camera!", message: "This application does not have permission to use camera. Please update your privacy settings.", preferredStyle: UIAlertControllerStyle.Alert)
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeAudio, completionHandler: { granted in
                    
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        if !granted {
                            var alertController: UIAlertController = UIAlertController(title: "Could not use sound!", message: "This application does not have permission to use sound. Please update your privacy settings.", preferredStyle: UIAlertControllerStyle.Alert)
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                        self.setupCamera()
                        var zoomFactor = self.cameraSessionController?.setVideoDeviceFormatWithZoomSupported()
                        
                        self.setupCompressionSession()
                        self.cameraSessionController?.startCamera()
                        
                        self.tcpVideoController.delegate = self
                        self.tcpVideoController.startListening(self.videoPort ?? 9092)
                        
                        self.tcpAudioController.delegate = self
                        self.tcpAudioController.startListening(self.audioPort ?? 9093)
                        
                        self.sliderZoomControl.minimumValue = 1
                        var max: CGFloat = zoomFactor ?? 1
                        self.sliderZoomControl.maximumValue = Float(max)
                    })
                })
            })
        });
    }
    
    
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK Action methods
    
    @IBAction func buttonStopTouchUpInside(sender: AnyObject) {
        
        videoCompressionSession?.delegate = nil
        audioCompressionSession?.delegate = nil
        
        cameraSessionController?.delegate = nil
        cameraSessionController?.stopCamera()
        
        tcpVideoController.delegate = nil
        tcpVideoController.stopListening()
        tcpVideoController.stopOutputStreams()
        
        tcpAudioController.delegate = nil
        tcpAudioController.stopListening()
        tcpAudioController.stopOutputStreams()
        
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func sliderZoomValueChanged(sender: AnyObject) {
        
        cameraSessionController?.setZoomLevel(CGFloat(self.sliderZoomControl.value))
    }
    
    
    // MARK Helper methods
    
    func setupCamera() {
        
        cameraSessionController = CameraSessionController()
        cameraSessionController?.delegate = self
        
        var cameraLayer = cameraView.layer as AVCaptureVideoPreviewLayer;
        cameraLayer.session = cameraSessionController?.session
        cameraLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
    }
    
    func setupCompressionSession() {
        
        self.videoCompressionSession = VideoCompressionSession()
        self.videoCompressionSession?.delegate = self
        
        var width = Int32(self.cameraView.frame.size.width * self.cameraView.contentScaleFactor)
        var height = Int32(self.cameraView.frame.size.height * self.cameraView.contentScaleFactor)
        
        var status = self.videoCompressionSession?.createCompressionSessionForWidth(width, height: height)
        println("create status \(status)")
        
        self.audioCompressionSession = AudioCompressionSession()
        self.audioCompressionSession?.delegate = self
    }
    
    
    // MARK CameraSessionControllerDelegate methods
    
    func cameraSessionDidOutputSampleVideoBuffer(sampleBuffer: CMSampleBuffer!) {
        
        var imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        if let imageBuffer = imageBuffer {
            var presentationTimeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            var duration = CMSampleBufferGetDuration(sampleBuffer)
            var status = VTCompressionSessionEncodeFrame(self.videoCompressionSession?.compressionSessionRef, imageBuffer, presentationTimeStamp, duration, nil, nil, nil)
        }
    }
    
    func cameraSessionDidOutputSampleAudioBuffer(sampleBuffer: CMSampleBuffer!) {

        self.audioCompressionSession?.encodeSampleBuffer(sampleBuffer)
    }
    
    
    // MARK VideoCompressionSessionDelegate methods
    
    func compressionSession(session: VideoCompressionSession!, didEncodeSampleBuffer sampleBuffer: CMSampleBuffer) {

        var attachments: NSArray? = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, 0)
        var pts: CMTime  = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        
        var isKeyframe: Bool = false
        if let attachments = attachments {
            if (attachments.count > 0) {
                var attachment: NSDictionary = attachments[0] as NSDictionary
                var key = kCMSampleAttachmentKey_DependsOnOthers as NSString
                var dependsOnOthers: Int? = attachment[key] as? Int
                isKeyframe = (dependsOnOthers != 0)
            }
        }
        if isKeyframe {
            var format: CMFormatDescriptionRef = CMSampleBufferGetFormatDescription(sampleBuffer);
            var spsSize: UInt = 0
            var ppsSize: UInt = 0
            var parmCount: UInt = 0
            var sps: UnsafePointer<UInt8> = nil
            var pps: UnsafePointer<UInt8> = nil
            
            CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format, UInt(0), &sps, &spsSize, &parmCount, nil);
            CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format, UInt(1), &pps, &ppsSize, &parmCount, nil);
            
            tcpVideoController.writeToOutputStream(sps, dataSize: spsSize)
            tcpVideoController.writeToOutputStream(pps, dataSize: ppsSize)
        }
        var bufferData: UnsafeMutablePointer<Int8> = nil
        var size: UInt = 0
        var block: CMBlockBufferRef = CMSampleBufferGetDataBuffer(sampleBuffer)
        CMBlockBufferGetDataPointer(block, 0, nil, &size, &bufferData)
        
        // Add start code to NAL unit
        var bufferDataWithStartCode = UnsafeMutablePointer<Void>(malloc(size + 4))
        memcpy(bufferDataWithStartCode.advancedBy(4), bufferData, size);
        var startCode = 1
        memcpy(bufferDataWithStartCode, &startCode, 4);
        
        tcpVideoController.writeToOutputStream(UnsafeMutablePointer<UInt8>(bufferDataWithStartCode), dataSize: size + 4)
        free(bufferDataWithStartCode)
    }

    
    // MARK AudioCompressionSessionDelegate methods
    
    func compressionSession(session: AudioCompressionSession!, didEncodeSampleBuffer sampleBuffer: UnsafeMutablePointer<Void>, size: UInt32) {
    
        // Add start code to ADTS packet

        var adtsLength = UInt(7);
        var packet = UnsafeMutablePointer<UInt>(malloc(adtsLength * UInt(sizeof(UInt))));

        var profile = UInt(2)
        var freqIdx = UInt(4)
        var chanCfg = UInt(1)
        var fullLength = UInt(adtsLength) + UInt(size);
        
        packet[0] = 0xFF
        packet[1] = 0xF9
        packet[2] = (((profile - 1) << 6) + (freqIdx << 2) + (chanCfg >> 2))
        packet[3] = (((chanCfg & 3) << 6) + (fullLength >> 11))
        packet[4] = ((fullLength & 0x7FF) >> 3)
        packet[5] = (((fullLength & 7) << 5) + 0x1F)
        packet[6] = 0xFC
        
        var bufferData: UnsafeMutablePointer<Int8> = nil
        
        var bufferDataWithADTS = UnsafeMutablePointer<Void>(malloc(Int(size) + adtsLength))
        memcpy(bufferDataWithADTS.advancedBy(Int(adtsLength)), sampleBuffer, UInt(size));
        memcpy(bufferDataWithADTS, packet, adtsLength);
        
        tcpAudioController.writeToOutputStream(UnsafeMutablePointer<UInt8>(bufferDataWithADTS), dataSize: UInt(fullLength))
        free(bufferDataWithADTS)
    }

    
    // MARK - TCPControllerDelegate methods
    
    func tcpController(controller: TCPController, didChangeConnectionNumber number: Int) {
        
        dispatch_async(dispatch_get_main_queue()) {
            if controller == self.tcpAudioController {
                self.labelAudioClientsConnected.text = "audio clients: \(number)"
            } else if controller == self.tcpVideoController {
                self.labelVideoClientsConnected.text = "video clients: \(number)"
            }
        }
    }
}
